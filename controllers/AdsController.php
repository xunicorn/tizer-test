<?php

namespace app\controllers;

use app\models\AdsLogs;
use Yii;
use app\models\Ads;
use app\models\AdsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * AdsController implements the CRUD actions for Ads model.
 */
class AdsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ads models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $browsers = [1,2,3,4,5];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'browsers'     => $browsers,
            'browsers_checked' => isset(Yii::$app->request->queryParams['browsers']) ? Yii::$app->request->queryParams['browsers'] : [],
        ]);
    }

    public function actionGetTizers($count = 6) {
        $this->layout = false;

        $records = Ads::getAds($count);

        if(!empty($records)) {
            foreach($records as $_record) {
                $browser_id = mt_rand(1,5);

                $_record->scenario = Ads::SCENARIO_UPDATE_COUNTER;

                $_record->counter = $_record->counter + 1;
                if(!$_record->save()) {
                    print_r($_record->getErrors());
                }

                AdsLogs::createLog($_record->id, $browser_id);
            }
        }

        return $this->renderPartial('getTizers', ['records' => $records]);
    }

    /**
     * Creates a new Ads model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ads();

        if ($model->load(Yii::$app->request->post())) {
            $model->scenario = Ads::SCENARIO_SAVE;
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');

            if($model->create()) {
                return $this->redirect(['index']);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Ads model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->scenario = Ads::SCENARIO_SAVE;

            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');

            if($model->create()) {
                return $this->redirect(['index']);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Ads model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ads model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Ads the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ads::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
