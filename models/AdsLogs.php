<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ads_logs".
 *
 * @property string $id
 * @property integer $ads_id
 * @property integer $browser_id
 * @property integer $date
 */
class AdsLogs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ads_logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ads_id', 'browser_id', 'date'], 'required'],
            [['ads_id', 'browser_id', 'date'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ads_id' => 'Ads ID',
            'browser_id' => 'Browser ID',
            'date' => 'Date',
        ];
    }

    public static function createLog($ads_id, $browser_id) {
        $model = new AdsLogs();

        $model->ads_id     = $ads_id;
        $model->browser_id = $browser_id;
        $model->date       = time();

        $model->save();
    }

    /**
     * @param $ads_id
     * @param $browsers
     * @return int|string
     */
    public static function getCount($ads_id, $browsers) {
        return AdsLogs::find()
            ->where('ads_id=:ads_id', [':ads_id' => $ads_id])
            ->andWhere(['in', 'browser_id', $browsers])
            ->count();
    }
}
