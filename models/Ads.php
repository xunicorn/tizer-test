<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\web\UploadedFile;

/**
 * This is the model class for table "ads".
 *
 * @property string $id
 * @property string $image_url
 * @property string $image_path
 * @property string $text
 * @property string $url
 * @property integer $browser_id
 * @property integer $counter
 * @property AdsLogs[] $logs
 */
class Ads extends \yii\db\ActiveRecord
{
    const SCENARIO_SAVE           = 'save_ads';
    const SCENARIO_UPDATE_COUNTER = 'update_counter_ads';

    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'url'], 'required'],
            [[ 'counter'], 'integer'],
            [['image_url', 'image_path', 'text', 'url'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    public function scenarios()
    {
        $scenarios =  parent::scenarios();

        $scenarios[Ads::SCENARIO_SAVE] = ['imageFile', 'text', 'url'];
        $scenarios[Ads::SCENARIO_UPDATE_COUNTER] = ['counter'];

        return $scenarios;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image_url' => 'Image',
            'image_path' => 'Image Path',
            'text' => 'Text',
            'url' => 'Url',
            'counter' => 'Counter',
        ];
    }

    public function create() {
        if ($this->validate()) {
            if(!$this->isNewRecord) {
                unlink($this->image_path);
            }

            $this->save();



            $imageDir    = Yii::getAlias('@webroot/') . Yii::$app->params['imageDir'];
            $imageDirUrl = (isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : 'http') . '://' . $_SERVER['SERVER_NAME'] . '/' . Yii::$app->params['imageDir'];

            if(!file_exists($imageDir)) {
                mkdir($imageDir);
            }

            $file_name = $this->id . '_' . $this->imageFile->baseName . '.' . $this->imageFile->extension;

            $this->imageFile->saveAs($imageDir . '/' . $file_name);

            $this->image_url  = $imageDirUrl . '/' . $file_name;
            $this->image_path = $imageDir . '/' . $file_name;

            $this->save(false);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $counter
     * @return Ads[]
     */
    public static function getAds($counter) {
        return Ads::find()->orderBy(new Expression('RAND()'))->limit($counter)->all();
    }

    public function getLogs() {
        return $this->hasMany(AdsLogs::className(), ['ads_id' => 'id']);
    }
}
