<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ads;
use yii\db\Expression;

/**
 * AdsSearch represents the model behind the search form about `app\models\Ads`.
 */
class AdsSearch extends Ads
{
    public $counter_by_browser;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'counter', 'counter_by_browser'], 'integer'],
            [['image_url', 'text', 'url'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ads::find();
        $query->alias('t');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'image_url', $this->image_url])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'url', $this->url]);

        if(isset($params['browsers'])) {
            $query->joinWith(['logs l'])->andWhere(['in', 'l.browser_id', $params['browsers']]);

            $subquery =AdsLogs::find()->select('ads_id, COUNT(subq.id) as counter_by_browser')->alias('subq')->where(['in', 'subq.browser_id', $params['browsers']])->groupBy('subq.ads_id');
            $query->leftJoin(['T' => $subquery], 'T.ads_id=t.id');

            $query->andFilterWhere([
                'counter_by_browser' => $this->counter,
            ]);
        } else {
            $query->andFilterWhere([
                'counter' => $this->counter,
            ]);
        }


        return $dataProvider;
    }
}