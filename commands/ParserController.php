<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.10.2016
 * Time: 11:02
 */

namespace app\commands;


use Yii;
use yii\console\Controller;

class ParserController extends Controller {
    private $url = 'http://kvmde02-7984.fornex.org:6677/index.php';

    private $cookiePath = '/cookies/cookie.txt';

    public function actionIndex() {
        $this->cookiePath = __DIR__ . $this->cookiePath;

        if(!file_exists(dirname($this->cookiePath))) {
            mkdir(dirname($this->cookiePath));
        }

        $return = $this->_request();

        $patt = '#<input.*name="_csrf".*value="(.*)".*>#U';

        if(!preg_match($patt, $return, $match)) {
            echo 'Hidden input not found' . PHP_EOL;
            return;
        }

        echo "========== CSRF: {$match[1]} ===========\n";

        $post = array(
            '_csrf' => $match[1],
        );

        $return = $this->_request($post);

        $patt = '#<table.*>\s*<tr>\s*<td>(.*)</td>\s*<td>(.*)</td>\s*<td>(.*)</td>.*#Uim';

        if(!preg_match($patt, $return, $match)) {
            echo 'No data found' . PHP_EOL;
            return;
        }


        print_r($match);
        //print_r(array_map('strip_tags', $match));

        $values = array();

        $patt = '#[\d\.\s]+#';

        for($i = 1; $i < 4; $i++) {
            if(preg_match($patt, $match[$i], $m)) {
                $values[] = str_replace(' ', '', $m[0]);
            }
        }

        print_r($values);

        $sql = "INSERT INTO parser (col_1, col_2, col_3) VALUES (" . implode(',', $values) . ")";

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        //$command->execute();
    }

    private function _request($post = array()) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);

        if(!empty($post)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiePath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiePath);

        $out = curl_exec($ch);
        curl_close($ch);

        return $out;
    }
} 