<?php
use app\models\Ads;

/* @var $records Ads[] */

?>

<div>
    <table>
    <?php foreach($records as $_rec): ?>
        <tr>
            <td>
                <?= \yii\helpers\Html::a(\yii\helpers\Html::img($_rec->image_url, ['style' => 'width: 120px;']), $_rec->url); ?>
            </td>
            <td>
                <?= $_rec->text; ?>
                <div></div>
                <?= \yii\helpers\Html::a($_rec->url, $_rec->url); ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </table>
</div>

