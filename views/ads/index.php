<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AdsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $browsers [] */
/* @var $browsers_checked [] */

$this->title = 'Ads';
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="ads-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ads', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Get Tizers', ['get-tizers'], ['class' => 'btn btn-info pull-right', 'target' => '_blank']) ?>
    </p>

    <div class="container-fluid">
        <div class="browser-filter well col-xs-3">
            <div class="head">Browser Filter</div>
            <div class="body">
                <?php Pjax::begin(); ?>
                    <?= Html::beginForm(['ads/index'], 'get'); ?>
                        <?= Html::checkboxList('browsers', $browsers_checked, array_combine($browsers, $browsers), ['separator' => ' ']); ?>

                        <div class="action">
                            <?= Html::submitButton('Apply filter', ['class' => 'btn'])?>
                        </div>
                    <?= Html::endForm(); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [ 'attribute' => 'id', 'contentOptions' => [ 'class' => 'id-column'], 'headerOptions' => [ 'class' => 'id-column'], 'filterOptions' => [ 'class' => 'id-column'] ],
            [
                'attribute' => 'image_url',
                'format' => 'raw',
                'value'  => function($model) { return Html::img($model->image_url, ['class' => 'tizer-img']); },
            ],
            'text',
            'url:url',
            [
                'attribute' => 'counter',
                'format' => 'raw',
                'value' => function($model) use($browsers_checked) {
                    if(!empty($browsers_checked)) {
                        return \app\models\AdsLogs::getCount($model->id, $browsers_checked);
                    } else {
                        return $model->counter;
                    }
                },
                'contentOptions' => [ 'class' => 'small-column'], 'headerOptions' => [ 'class' => 'small-column'], 'filterOptions' => [ 'class' => 'small-column'] ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete}'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

