<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p>
            <?= \yii\helpers\Html::a('Lets start create ADS', ['ads/index'], ['class' => 'btn btn-success btn-lg']); ?>
        </p>
    </div>


</div>
